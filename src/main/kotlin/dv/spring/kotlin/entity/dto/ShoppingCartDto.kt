package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(var shoppingCartStatus: ShoppingCartStatus = ShoppingCartStatus.WAIT,
                           var selectedProduct: List<SelectedProductDto> = mutableListOf<SelectedProductDto>(),
                           var customer: CustomerDto? = null
)