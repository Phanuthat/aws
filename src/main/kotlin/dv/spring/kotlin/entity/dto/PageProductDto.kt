package dv.spring.kotlin.entity.dto

import org.apache.commons.math3.stat.descriptive.summary.Product

data class PageProductDto(var totalPage: Int? = null,
                          var totalElements: Long? = null,
                          var products: List<ProductDto> = mutableListOf())