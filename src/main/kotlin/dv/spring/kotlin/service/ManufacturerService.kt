package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.dto.ManufacturerDto

interface ManufacturerService {
    fun getManufacturers(): List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer

}
